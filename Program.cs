﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThirdLessonOTUS
{
    public class Program
    {
        static void Main(string[] args)
        {
            var list = new LinkedList<int>()
            {
                2, 1, 3, 5, 42, -69, 17, -71, -6
            };

            Console.Write("Created list: ");
            foreach (var item in list)
                Console.Write(item + ", ");

            Console.WriteLine();

            int toRemove = 5;
            list.Remove(toRemove);
            Console.Write("Removed " + toRemove + ": ");
            foreach (var item in list)
                Console.Write(item + ", ");

            Console.WriteLine();

            Console.Write("Sorted list: ");
            list.SortFromSmallest(ref list);
            list.Compare(1, 1);

            foreach (var item in list)
                Console.Write(item + ", ");

            int ToFind = -69;
            Console.Write($"\nIndex of {ToFind} is {list.Search(ToFind, list)} (do not forget that indexing starts with '0')");

            Console.ReadLine();
        }
    }
}
