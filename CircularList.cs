﻿using System;
using System.Collections;
using System.Diagnostics.CodeAnalysis;

namespace ThirdLessonOTUS
{
    public interface IAlgorithm<T>
    {
        void SortFromSmallest(ref LinkedList<int> list);
        int Search(int valueTofind, LinkedList<int> list);
    }

    public class Item<T>
    {
        public T Data { get; set; }
        public Item<T> Next { get; set; }

        public Item(T data)
        {
            if (data == null)
                throw new ArgumentNullException(nameof(data));
            Data = data;
        }

        public override string ToString()
        {
            return Data.ToString();
        }
    }

    public class LinkedList<T> : IEnumerable, IAlgorithm<T> where T : IComparable<T>
    {
        private Item<T> _head = null;
        private Item<T> _tail = null;

        public int Count { get; private set; } = 0;

        public void Add(T data)
        {
            if (data == null)
                throw new ArgumentNullException();

            var item = new Item<T>(data);

            if (_head == null)
                _head = item;
            else
                _tail.Next = item;

            _tail = item;
            Count++;
        }

        public void Remove(int data)
        {
            if (data == 0)
                throw new ArgumentNullException(nameof(data));

            var current = _head;
            Item<T> previous = _head;

            while (current != null)
            {
                if (current.Data.Equals(data))
                {
                    if (previous != null)
                    {
                        previous.Next = current.Next;
                        if (current.Next == null)
                            _tail = previous;
                    }
                    Count--;
                    break;
                }
                previous = current;
                current = current.Next;
            }
        }

        public void Clear()
        {
            _head = null;
            _tail = null;
            Count = 0;
        }

        public IEnumerator GetEnumerator()
        {
            var current = _head;
            while (current != null)
            {
                yield return current.Data;
                current = current.Next;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)this).GetEnumerator();
        }

        private int BinarySearch(int[] array, int valueToFind, int firstIndex, int lastIndex)
        {
            if (firstIndex > lastIndex)
                return -1;

            var middle = (firstIndex + lastIndex) / 2;
            var middleValue = array[middle];

            if (middleValue == valueToFind)
                return middle;
            else if (middleValue > valueToFind)
                return BinarySearch(array, valueToFind, firstIndex, middle - 1);
            else
                return BinarySearch(array, valueToFind, middle + 1, lastIndex);
        }

        private int[] ConvertToArray(LinkedList<int> list)
        {
            int[] arr = new int[Count];
            var current1 = list._head;
            for (int i = 0; i < Count; i++)
            {
                arr[i] = current1.Data;
                current1 = current1.Next;
            }
            return arr;
        }

        public void SortFromSmallest(ref LinkedList<int> list)
        {
            int[] arr = ConvertToArray(list);
            Array.Sort(arr);
            LinkedList<int> newList = new LinkedList<int>();
            for (int i = 0; i < arr.Length; i++)
                newList.Add(arr[i]);
            list._head = newList._head;
            list._tail = newList._tail;
        }

        public int Search(int valueTofind, LinkedList<int> list)
        {
            int[] arr = ConvertToArray(list);
            Array.Sort(arr);
            return BinarySearch(arr, valueTofind, 0, arr.Length - 1);
        }

        public bool Compare(T a, T b)
        {
            return a.CompareTo(b) == 0;
        }
    }
}
